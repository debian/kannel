From: Helmut Grohne <helmut@subdivi.de>
Date: Fri, 27 Sep 2024 23:19:10 +0200
Subject: Fix cross-building

kannel fails to cross build from source for two reasons. One is its use
of AC_CHECK_FILE. The macro is meant to check for files on the host
system, but kannel exclusively uses it for checking build system files.
For that task, a simple test -e is the solution. The other issue is that
it misdetects presence of mysql. During cross compilation mysql_config
does not work. Please use pkg-config instead.
---
 configure.in | 33 ++++++++++++++++++++++-----------
 1 file changed, 22 insertions(+), 11 deletions(-)

diff --git a/configure.in b/configure.in
index 5a767ca..1d164c2 100644
--- a/configure.in
+++ b/configure.in
@@ -489,7 +489,7 @@ for loc in /usr /usr/local /sw /opt/local; do
         ${loc}/share/sgml/dsssl/docbook-dsssl-nwalsh/html/docbook.dsl \
         ${loc}/share/dsssl/docbook-dsssl/html/docbook.dsl ; do
       if test "x$found" = "x" ; then 
-	AC_CHECK_FILE($file,HTML_DSL=$file; found=1)
+	AS_IF([test -e "$file"],HTML_DSL=$file; found=1)
       fi
     done
   fi
@@ -507,7 +507,7 @@ for loc in /usr /usr/local /sw /opt/local; do
         ${loc}/share/sgml/dsssl/docbook-dsssl-nwalsh/print/docbook.dsl \
         ${loc}/share/dsssl/docbook-dsssl/print/docbook.dsl ; do
       if test "x$found" = "x" ; then 
-	AC_CHECK_FILE($file,TEX_DSL=$file; found=1)
+	AS_IF([test -e "$file"],TEX_DSL=$file; found=1)
       fi
     done
   fi    
@@ -526,7 +526,7 @@ for loc in /usr /usr/local /sw /opt/local; do
         ${loc}/share/sgml/dsssl/docbook-dsssl-nwalsh/dtds/decls/xml.dcl \
         ${loc}/share/dsssl/docbook-dsssl/dtds/decls/xml.dcl ; do
       if test "x$found" = "x" ; then 
-    AC_CHECK_FILE($file,XML_DCL=$file; found=1)
+        AS_IF([test -e "$file"],XML_DCL=$file; found=1)
       fi
     done
   fi
@@ -988,6 +988,16 @@ else
     ])
     
     AC_MSG_RESULT(searching)
+    mysql_found=no
+    AS_IF([test "x$mysqlloc" = x],,[
+      PKG_CHECK_MODULES([MYSQLCLIENT],[mysqlclient],[
+        CFLAGS="$CFLAGS $MYSQLCLIENT_CFLAGS"
+        LIBS="$LIBS $MYSQLCLIENT_LIBS"
+	mysql_found=yes
+      ])
+    ])
+
+    if test "$mysql_found" = no; then
     AC_PATH_PROG(MYSQL_CONFIG, mysql_config, no, [$PATH:$mysqlloc/bin:$mysqlloc])
     dnl check for MySQL 4.x style mysql_config information
     if test "$MYSQL_CONFIG" = "no"; then
@@ -996,9 +1006,9 @@ else
         if test "x$found" = "x" ; then
             AC_MSG_CHECKING([for MySQL client support in])
             AC_MSG_RESULT($loc)
-            AC_CHECK_FILE("$loc/include/mysql/mysql.h",
+            AS_IF([test -e "$loc/include/mysql/mysql.h"],
             [CFLAGS="$CFLAGS -I$loc/include/mysql"; LIBS="$LIBS -L$loc/lib/mysql -lmysqlclient"]; found=1,
-            [AC_CHECK_FILE("$loc/include/mysql.h",
+            [AS_IF([test -e "$loc/include/mysql.h"],
                 [CFLAGS="$CFLAGS -I$loc/include"; LIBS="$LIBS -L$loc/lib -lmysqlclient"]; found=1
             )]
             )
@@ -1038,6 +1048,7 @@ else
         CFLAGS="$CFLAGS $MYSQL_CFLAGS"
         AC_MSG_RESULT([$MYSQL_CFLAGS])
     fi
+    fi
     AC_CHECK_HEADERS(mysql/mysql.h mysql/mysql_version.h)
     AC_CHECK_LIB(mysqlclient_r, mysql_stmt_init, [],
 		AC_CHECK_LIB(mysqlclient, mysql_stmt_init, [], [AC_MSG_ERROR([Unable to find MySQL client libraries version >= 4.1])])
@@ -1236,11 +1247,11 @@ else
     		if test "x$found" = "x" ; then
     			AC_MSG_CHECKING([for PostgresSQL include files in])
     			AC_MSG_RESULT($loc)
-    			AC_CHECK_FILE("$loc/include/postgresql/libpq-fe.h",
+    			AS_IF([test -e "$loc/include/postgresql/libpq-fe.h"],
             	  [CFLAGS="$CFLAGS -I$loc/include/postgresql"; LIBS="$LIBS -L$loc/lib/postgresql -lpq"]; found=1,
-            	  [AC_CHECK_FILE("$loc/include/pgsql/libpq-fe.h",
+            	  [AS_IF([test -e "$loc/include/pgsql/libpq-fe.h"],
             	    [CFLAGS="$CFLAGS -I$loc/include/pgsql"; LIBS="$LIBS -L$loc/lib/pgsql -lpq"]; found=1,
-            	    [AC_CHECK_FILE("$loc/pgsql/include/libpq-fe.h",
+            	    [AS_IF([test -e "$loc/pgsql/include/libpq-fe.h"],
             	    [CFLAGS="$CFLAGS -I$loc/pgsql/include"; LIBS="$LIBS -L$loc/pgsql/lib -lpq"]; found=1,
                   )]
                 )])
@@ -1309,9 +1320,9 @@ else
             if test "x$found" = "x" ; then
                 AC_MSG_CHECKING([for Redis include files in])
                 AC_MSG_RESULT($loc)
-                AC_CHECK_FILE("$loc/hiredis.h",
+                AS_IF([test -e "$loc/hiredis.h"],
                   [CFLAGS="$CFLAGS -I$loc"; LIBS="$LIBS -L$loc -lhiredis"]; found=1,
-                  [AC_CHECK_FILE("$loc/include/hiredis/hiredis.h",
+                  [AS_IF([test -e "$loc/include/hiredis/hiredis.h"],
                     [CFLAGS="$CFLAGS -I$loc/include/hiredis"; LIBS="$LIBS -L$loc/lib -lhiredis"]; found=1
                   )]
                 )
@@ -1407,7 +1418,7 @@ else
         if test "x$found" = "x" ; then
             AC_MSG_CHECKING([for Cassandra include files in])
             AC_MSG_RESULT($loc)
-            AC_CHECK_FILE("$loc/include/cassandra.h",
+            AS_IF([test -e "$loc/include/cassandra.h"],
               [CFLAGS="$CFLAGS -I$loc/include"; LIBS="$LIBS -L$loc/lib -lcassandra"]; found=1
             )
         fi
